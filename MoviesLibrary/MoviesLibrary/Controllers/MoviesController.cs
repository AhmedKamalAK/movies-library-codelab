﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MoviesLibrary.Data;
using MoviesLibrary.Models;
using System.Web.Http.Cors;

namespace MoviesLibrary.Controllers
{
    //[EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    public class MoviesController : ApiController
    {
        private MoviesLibraryContext db = new MoviesLibraryContext();

        // GET api/Movies
        public IEnumerable<FilmViewModel> GetFilms()
        {
            List<FilmViewModel> filmsModels = new List<FilmViewModel>();
            foreach (var film in db.Films)
            {
                filmsModels.Add(new FilmViewModel()
                {
                    FilmId = film.FilmId,
                    FilmName = film.FilmName,
                    PictureURL = film.PictureURL
                });
            }

            return filmsModels;
        }

        // GET api/Movies/5
        [ResponseType(typeof(FilmDetailsViewModel))]
        public IHttpActionResult GetMovie(Guid id)
        {
            Film film = db.Films.Find(id);
            if (film == null)
            {
                return NotFound();
            }
            
            List<MemberViewModel> filmMembersData = new List<MemberViewModel>();

            foreach (var filmMemberRole in film.FilmMemberRoles)
            {
                filmMembersData.Add(new MemberViewModel()
                {
                    MemberName = filmMemberRole.Member.MemberName,
                    Role = filmMemberRole.Role.RoleName
                });
            }

            FilmDetailsViewModel filmDetails = new FilmDetailsViewModel()
            {
                FilmId = film.FilmId,
                FilmName = film.FilmName,
                PictureURL = film.PictureURL,
                FilmMembers = filmMembersData
            };

            return Ok(filmDetails);
        }

        // PUT api/Films/5
        //public IHttpActionResult PutFilm(Guid id, Film film)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != film.FilmId)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(film).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!FilmExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST api/Films
        //[ResponseType(typeof(Film))]
        //public IHttpActionResult PostFilm(Film film)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Films.Add(film);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (FilmExists(film.FilmId))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return CreatedAtRoute("DefaultApi", new { id = film.FilmId }, film);
        //}

        //// DELETE api/Films/5
        //[ResponseType(typeof(Film))]
        //public IHttpActionResult DeleteFilm(Guid id)
        //{
        //    Film film = db.Films.Find(id);
        //    if (film == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Films.Remove(film);
        //    db.SaveChanges();

        //    return Ok(film);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FilmExists(Guid id)
        {
            return db.Films.Count(e => e.FilmId == id) > 0;
        }
    }
}