namespace MoviesLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FilmMemberRoles",
                c => new
                    {
                        FilmId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                        MemberId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.FilmId, t.RoleId, t.MemberId })
                .ForeignKey("dbo.Films", t => t.FilmId, cascadeDelete: true)
                .ForeignKey("dbo.Members", t => t.MemberId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.FilmId)
                .Index(t => t.RoleId)
                .Index(t => t.MemberId);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        FilmId = c.Guid(nullable: false),
                        FilmName = c.String(),
                    })
                .PrimaryKey(t => t.FilmId);
            
            CreateTable(
                "dbo.Members",
                c => new
                    {
                        MemberId = c.Guid(nullable: false),
                        MemberName = c.String(),
                    })
                .PrimaryKey(t => t.MemberId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Guid(nullable: false),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FilmMemberRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.FilmMemberRoles", "MemberId", "dbo.Members");
            DropForeignKey("dbo.FilmMemberRoles", "FilmId", "dbo.Films");
            DropIndex("dbo.FilmMemberRoles", new[] { "MemberId" });
            DropIndex("dbo.FilmMemberRoles", new[] { "RoleId" });
            DropIndex("dbo.FilmMemberRoles", new[] { "FilmId" });
            DropTable("dbo.Roles");
            DropTable("dbo.Members");
            DropTable("dbo.Films");
            DropTable("dbo.FilmMemberRoles");
        }
    }
}
