namespace MoviesLibrary.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FilmMemberRole
    {
        [Key]
        [Column(Order = 0)]
        public Guid FilmId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid RoleId { get; set; }

        [Key]
        [Column(Order = 2)]
        public Guid MemberId { get; set; }

        public virtual Film Film { get; set; }

        public virtual Member Member { get; set; }

        public virtual Role Role { get; set; }
    }
}
