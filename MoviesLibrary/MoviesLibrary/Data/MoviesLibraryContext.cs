namespace MoviesLibrary.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MoviesLibraryContext : DbContext
    {
        public MoviesLibraryContext()
            : base("name=MoviesLibraryContext")
        {
        }

        public virtual DbSet<FilmMemberRole> FilmMemberRoles { get; set; }
        public virtual DbSet<Film> Films { get; set; }
        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
