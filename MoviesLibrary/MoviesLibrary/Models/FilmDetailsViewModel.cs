﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviesLibrary.Models
{
    public class FilmDetailsViewModel
    {
        public Guid FilmId { get; set; }
        public string FilmName { get; set; }
        public string PictureURL { get; set; }
        public IEnumerable<MemberViewModel> FilmMembers { get; set; }

        public FilmDetailsViewModel()
        {
            FilmMembers = new List<MemberViewModel>();
        }
    }
}