﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviesLibrary.Models
{
    public class FilmViewModel
    {
        public Guid FilmId { get; set; }
        public string FilmName { get; set; }
        public string PictureURL { get; set; }
    }
}