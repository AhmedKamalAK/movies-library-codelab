﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviesLibrary.Models
{
    public class MemberViewModel
    {
        public string MemberName { get; set; }
        public string Role { get; set; }
    }
}