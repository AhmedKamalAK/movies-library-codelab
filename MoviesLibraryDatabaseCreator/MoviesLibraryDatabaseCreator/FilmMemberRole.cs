﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibraryDatabaseCreator
{
    class FilmMemberRole
    {
        [Key]
        [Column(Order = 0)]
        public Guid FilmId { get; set; }
        [Key]
        [Column(Order = 1)]
        public Guid RoleId { get; set; }
        [Key]
        [Column(Order = 2)]
        public Guid MemberId { get; set; }

        [ForeignKey("FilmId")]
        public Film Film { get; set; }
        [ForeignKey("RoleId")]
        public Role Role { get; set; }
        [ForeignKey("MemberId")]
        public Member Member { get; set; }
    }
}
