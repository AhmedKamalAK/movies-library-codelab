﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibraryDatabaseCreator
{
    class MoviesLibraryContext : DbContext
    {
        public DbSet<Film> Films { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<FilmMemberRole> FilmsMembersRoles { get; set; }

        public MoviesLibraryContext() : base("MoviesLibrary")
        { }
    }
}
