﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibraryDatabaseCreator
{
    class Role
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public virtual ICollection<FilmMemberRole> FilmMemberRoles { get; set; }

        public Role()
        {
            FilmMemberRoles = new List<FilmMemberRole>();
        }
    }
}
