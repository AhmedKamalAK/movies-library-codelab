﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibraryDatabaseCreator
{
    class Member
    {
        public Guid MemberId { get; set; }
        public string MemberName { get; set; }
        public virtual ICollection<FilmMemberRole> FilmMembersRole { get; set; }

        public Member()
        {
            FilmMembersRole = new List<FilmMemberRole>();
        }
    }
}
