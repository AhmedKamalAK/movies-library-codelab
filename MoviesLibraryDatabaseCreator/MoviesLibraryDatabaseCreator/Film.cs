﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibraryDatabaseCreator
{
    class Film
    {
        public Guid FilmId { get; set; }
        public string FilmName { get; set; }
        public virtual ICollection<FilmMemberRole> FilmsMemberRole { get; set; }

        public Film()
        {
            FilmsMemberRole = new List<FilmMemberRole>();
        }
    }
}
