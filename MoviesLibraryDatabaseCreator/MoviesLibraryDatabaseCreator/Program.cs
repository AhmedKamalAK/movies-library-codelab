﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibraryDatabaseCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            MoviesLibraryContext moviesContext = new MoviesLibraryContext();

            string filmName = "Inception";
            string memberName = "Joseph Gordon-Levitt";
            string roleName = "Actor";

            Film film = moviesContext.Films.FirstOrDefault(f => f.FilmName == "Inception");
            foreach (var filmMember in film.FilmsMemberRole)
            {
                Console.WriteLine(moviesContext.Members.FirstOrDefault(m => m.MemberId == filmMember.MemberId).MemberName);
            }

            //moviesContext.Films.Add(new Film()
            //{
            //    FilmId = Guid.NewGuid(),
            //    FilmName = filmName
            //});
            //moviesContext.Members.Add(new Member()
            //{
            //    MemberId = Guid.NewGuid(),
            //    MemberName = memberName
            //});
            //moviesContext.Roles.Add(new Role()
            //{
            //    RoleId = Guid.NewGuid(),
            //    RoleName = roleName
            //});
            //moviesContext.SaveChanges();
            //moviesContext.FilmsMembersRoles.Add(new FilmMemberRole()
            //{
            //    FilmId = moviesContext.Films.FirstOrDefault(f => f.FilmName == filmName).FilmId,
            //    MemberId = moviesContext.Members.FirstOrDefault(f => f.MemberName == memberName).MemberId,
            //    RoleId = moviesContext.Roles.FirstOrDefault(f => f.RoleName == roleName).RoleId,

            //    //Film = moviesContext.Films.FirstOrDefault(f => f.FilmName == filmName),
            //    //Member = moviesContext.Members.FirstOrDefault(f => f.MemberName == memberName),
            //    //Role = moviesContext.Roles.FirstOrDefault(f => f.RoleName == roleName),
            //});

            //moviesContext.SaveChanges();
        }
    }
}
