import { Component, Input, OnInit } from 'angular2/core';
import { MoviesService } from './movies.service';
import { HTTP_PROVIDERS } from 'angular2/http';
import { RouteParams } from 'angular2/router';

@Component({
    selector: 'movie-details',
    templateUrl: 'app/movie-details.component.html',
    providers: [MoviesService, HTTP_PROVIDERS],
    styleUrls: ['app/movie-img.css']
})
export class MovieDetailsComponent implements OnInit {
    movieDetails: any;
    constructor(private _moviesService: MoviesService, private _routeParams : RouteParams){
        
    }

    ngOnInit() {
        this._moviesService.getMovie(this._routeParams.get("id"))
            .subscribe(m => {
                this.movieDetails = m;
                console.log(this.movieDetails.FilmMembers);
            });
    }
}