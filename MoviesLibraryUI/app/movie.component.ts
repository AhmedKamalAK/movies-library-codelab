import { Component, Input } from 'angular2/core';
import { ROUTER_DIRECTIVES } from 'angular2/router';

@Component({
    selector: 'movie',
    templateUrl: 'app/movie.component.html',
    styleUrls: ['app/movie.component.css', 'app/movie-img.css'],
    directives: [ROUTER_DIRECTIVES],
})
export class MovieComponent {
    @Input('movieInfo') film;
}