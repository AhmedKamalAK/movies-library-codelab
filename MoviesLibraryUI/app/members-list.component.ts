import { Component, Input } from 'angular2/core';

@Component({
    selector: 'members-list',
    template: `
                <tr *ngFor="#member of members">
                    <td>{{member.MemberName}}</td>
                    <td>{{member.Role}}</td>
                </tr>
    `
})
export class MembersListComponent {
    @Input('membersList') members;
}