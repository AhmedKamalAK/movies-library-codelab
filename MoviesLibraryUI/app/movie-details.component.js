System.register(['angular2/core', './movies.service', 'angular2/http', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, movies_service_1, http_1, router_1;
    var MovieDetailsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (movies_service_1_1) {
                movies_service_1 = movies_service_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            MovieDetailsComponent = (function () {
                function MovieDetailsComponent(_moviesService, _routeParams) {
                    this._moviesService = _moviesService;
                    this._routeParams = _routeParams;
                }
                MovieDetailsComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._moviesService.getMovie(this._routeParams.get("id"))
                        .subscribe(function (m) {
                        _this.movieDetails = m;
                        console.log(_this.movieDetails.FilmMembers);
                    });
                };
                MovieDetailsComponent = __decorate([
                    core_1.Component({
                        selector: 'movie-details',
                        templateUrl: 'app/movie-details.component.html',
                        providers: [movies_service_1.MoviesService, http_1.HTTP_PROVIDERS],
                        styleUrls: ['app/movie-img.css']
                    }), 
                    __metadata('design:paramtypes', [movies_service_1.MoviesService, router_1.RouteParams])
                ], MovieDetailsComponent);
                return MovieDetailsComponent;
            }());
            exports_1("MovieDetailsComponent", MovieDetailsComponent);
        }
    }
});
//# sourceMappingURL=movie-details.component.js.map