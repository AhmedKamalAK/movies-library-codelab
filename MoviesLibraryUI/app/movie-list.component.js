System.register(['angular2/core', './movie.component', './movies.service', 'angular2/http'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, movie_component_1, movies_service_1, http_1;
    var MovieListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (movie_component_1_1) {
                movie_component_1 = movie_component_1_1;
            },
            function (movies_service_1_1) {
                movies_service_1 = movies_service_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            MovieListComponent = (function () {
                function MovieListComponent(_moviesService) {
                    this._moviesService = _moviesService;
                }
                MovieListComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._moviesService.getMoviesList().subscribe(function (m) {
                        _this.movies = m;
                    });
                };
                MovieListComponent = __decorate([
                    core_1.Component({
                        selector: 'movie-list',
                        templateUrl: 'app/movie-list.component.html',
                        directives: [movie_component_1.MovieComponent],
                        providers: [movies_service_1.MoviesService, http_1.HTTP_PROVIDERS],
                    }), 
                    __metadata('design:paramtypes', [movies_service_1.MoviesService])
                ], MovieListComponent);
                return MovieListComponent;
            }());
            exports_1("MovieListComponent", MovieListComponent);
        }
    }
});
//# sourceMappingURL=movie-list.component.js.map