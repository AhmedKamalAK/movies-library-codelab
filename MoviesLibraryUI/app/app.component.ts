import {Component, OnInit} from 'angular2/core';
import { RouteConfig, ROUTER_DIRECTIVES } from 'angular2/router';

import { MoviesService } from './movies.service';
//import { MovieComponent } from './movie.component';
import { MovieDetailsComponent } from './movie-details.component';
import { MovieListComponent } from './movie-list.component';

@RouteConfig([
    { path: '/movies', name: 'Movies', component: MovieListComponent, useAsDefault: true },
    { path: '/movies/:id', name: 'MovieDetails', component: MovieDetailsComponent },
    { path: '/*other', name: 'Other', redirectTo: ['Movies']}
])
@Component({
    selector: 'my-app',
    directives: [ROUTER_DIRECTIVES],
    templateUrl: 'app/app.component.html',
})
export class AppComponent {
    
}