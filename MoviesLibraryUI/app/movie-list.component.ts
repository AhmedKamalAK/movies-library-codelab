import { Component, OnInit } from 'angular2/core';
import { MovieComponent } from './movie.component';
import { MoviesService } from './movies.service';
import { HTTP_PROVIDERS } from 'angular2/http';

@Component({
    selector: 'movie-list',
    templateUrl: 'app/movie-list.component.html',
    directives: [MovieComponent],
    providers: [MoviesService, HTTP_PROVIDERS],
})
export class MovieListComponent implements OnInit {
    movies;

    constructor (private _moviesService : MoviesService)
    {

    }

    ngOnInit() {
        this._moviesService.getMoviesList().subscribe(m => {
            this.movies = m;
        });
    }
}