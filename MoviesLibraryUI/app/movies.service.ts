import {Http, Headers, Response, RequestOptions, URLSearchParams} from 'angular2/http';
import 'rxjs/add/operator/map';
import { Injectable } from 'angular2/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MoviesService {
    constructor(private _http: Http) {
        
    }
    
    
    getMoviesList () {
        return this._http.get("http://localhost:6021/api/movies").map(m => m.json());
    }

    getMovie(id) {
        return this._http.get("http://localhost:6021/api/movies/" + id).map(m => m.json());
    }
}